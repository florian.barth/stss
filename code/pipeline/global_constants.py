"""This file contains global constants used in the SpaCy pipeline.
"""

# Maximum number of characters per document (default: 1000000)
SPACY_MAX_LENGTH = 12000000