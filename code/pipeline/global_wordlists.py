"""This file contains global wordlists used in the SpaCy pipeline.
"""

from num2words import num2words


# comment-indicating markers
COMMENT_MARKERS = [
	# Attitude
	'dafür', 
	'dagegen',

	# Argumentation
	'aufgrund', 
	'aufgrunddessen',
	'indem', 
	'da',
	#'dabei',
	'dadurch', #Disambiguiation: meaning argument not place
	'daher', 
	'damit', 
	#'darauf',
	#'daraufhin',
	'darum', 
	'denn', 
	'deshalb', 
	'deswegen', 
	'folglich',
	#'um', 
	'weil', 
	'weshalb', 
	'weswegen',

	#not always argumentative?
	#'hierauf',
	'hieraus', 
	'hierdurch', 
	'hiernach', 
	'ergo',

	# Opposite; opinion?
	'allerdings', 
	'anderenfalls',
	#'ansonsten',
	#'anstatt',
	#'anstelle',
	'einerseits', 
	'andererseits', 
	'andernfalls', 
	'andrerseits',
	'außerdem', 
	'demgegenüber', 
	'dennoch', 
	'dessenungeachtet',
	'eigentlich',
	'hingegen', 
	'jedoch', 
	'nichtsdestotrotz', 
	'nichtsdestoweniger', 
	'obgleich', 
	'obschon', 
	'obwohl', 
	'obzwar', 
	'statt', 
	'stattdessen', 
	'trotz', 
	'trotzdem', 
	'wogegen', 
	'wohingegen'
	'ansonsten', 
	#'entgegen',
	#'entweder',
	'sonst',

	#between sentences
	'aber',
	'sondern',
	#'ob',

	'wenn',
	'dann',
	#'sodann',

	'allein', # not allein + Verb 
	'eben',
	'erst',
	'fast', 
	'ganz',
	'gerade',
	'noch', #?
	'recht',
	'sogar',
	'zwar',

	# only between sentences
	#'und',
	#'oder',

	#'beinahe',
	#'beispielsweise', 
	'besonders', #besonders wenn, besonders + ADJ 
	#'bereits',
	#'bevor',
	#'beziehungsweise',
	#'bspw',
	#'bzw',
	'darüberhinaus', 
	'demnach', 
	'demzufolge',  
	'desgleichen',    
	'desweiteren',  
	'ebensowenig',       
	#'falls', 
	#'ferner',   
	#'genaugenommen',  
	'geradezu',
	'gezwungenermaßen', 
	#'gleichfalls', 
	#'gleichwohl', 
	'hoch', 
	'immer',
	'infolge', 
	'infolgedessen', 
	'insbesondere', 
	#'insofern', 
	#'inwiefern', 
	#'inwieweit', 
	#'inzwischen', 
	'mithin',  
	'nahezu', 
	'nachdem',
	#'nebenher' 
	'nur',    
	'obendrein',   
	#'ohnedass', 
	'ohnedies', 
	'ohnehin',  
	'sehr',  
	#'selbst', 
	'so', #?
	'sobald', 
	#'sodass', 
	'sofern',  
	'somit',  
	'umso', 
	'unterdessen', 
	'vielmehr',  
	'vorausgesetzt', #? 
	#'warum',
	#'während',
	#'weiterhin',
	'wegen',  
	'weitaus', # attitude?  
	'wenngleich', # attitude?
	#'wie',
	#'wonach', 
	#'worauf', 
	#'woraufhin',    
	#'zudem',
	#'zugleich',
	'zumal', 
	'zumindest', 
	#'zusätzlich',   
	'überaus', 
	'überdies',

#als
#außer
#etwas
#danach
#seit
#später
#zuvor
#zunächst
#zuvor
]


# comment-indicating modifiers
COMMENT_MODIFIERS = [
    'also',
	'allenfalls',
    'bekanntlich', 
    'bloß', 
    'doch', 
    'durchaus', 
    'ebenfalls', 
    'eher', 
    'einfach', 
    'einigermaßen', 
    'etwa', 
    'freilich',
	'gewiss',
    'gar', 
    'halt', 
    'immerhin', 
    'jedenfalls', 
    'lediglich', 
    'letztlich', 
    'natürlich', 
    'nämlich', 
    'offenbar', 
    'schließlich', 
    'schon', #?
    'selbstverständlich',
	'sicher',
    'sicherlich', 
    'sozusagen', 
    'tatsächlich', 
    'unbedingt', 
    'völlig', 
    'wohl', 
    'ziemlich', 
    'überhaupt', 
    'übrigens', 
    
    "auch", 
    "vielleicht",
]


# comment-indicating nouns
COMMENT_NOUNS = [
	'Annahme', 
	'Ansicht',
	'Bedenken',
	'Behauptung',
	'Bemerkung',
	'Beurteilung',
	'Bewertung',
	'Einstellung',
	'Erwartung', 
	'Fakt', 
	'Folgerung', 
	'Gedanke',
	#'Hoffnung' 
	'Idee', 
	'Konsequenz', 
	'Meinung', 
	'Position', 
	'Sichweise', 
	'Standpunkt', 
	'Überlegung', 
	'Überzeugung',
	'Vermutung',
]


# comment-indicating verbs
COMMENT_VERBS = [
	# + dass ?
	'annehmen',
	'bedenken', 
	'bemerken', 
	'beurteilen', 
	'bewerten', 
	'denken',
	'ergeben', 
	'erwarten', 
	'finden', 
	'glauben', 
	'hoffen', 
	'meinen', 
	'merken', 
	'rechnen', 
	'vermuten', 
	'wünschen', 
	'überlegen',
	'zeugen',
	'zwingen',

	'aüßern', 
	'behaupten', 
	'beschließen', 
	'ergeben', 
	'feststellen', 
	'folgern', 
	'schließen',
]


# deictic expressions
DEICTIC = [
    # pronouns - they don't seem to be helpful (not always deictic but sometimes anaphoric)
    # "ich", "du", "wir", "ihr", "sie", 
    # "mein", "meine", "meinen", "dies", "diese", "diesen", "jen", "jene", "jenen",
    
    # temporal
    "jetzt", "heute", "gestern", "morgen", "bald", "kürzlich", "vorhin", "vorher", "nachher", # "dann" not always temporal
    
    # local
    "hier", "dort", "da", "hinter", "links", "rechts", "oben", "unten", # "über" above and about, "vor" not always local
]

EVALUATIVE_TERMS = ['sonderbar', 'höchst']

# frequency adverbs that mark generic VPs
FREQUENT_ADV = [
    "stündlich", "täglich", "wöchentlich", "monatlich", "jährlich",
    "morgens", "vormittags", "mittags", "nachmittags", "abends", "nachts", 
    "montags", "dienstags", "mittwochs", "donnerstags", "freitags", "samstags", "sonnabends", "sonntags"
]


# negation words
NEGATIONS = [
    "kein",
    "keineswegs",
    "nicht",
    "nichts",
    "nie",
    "niemals",
    "niemand",
    "nirgends",
    "nirgendwo"
]


# quantifying numbers
NUMBERS = sum([
    [num2words(x, lang="de") for x in range(2, 20)],
    sum([[num2words(x, lang="de") for x in range(10**n, 10**(n+1), 10**n)] for n in range(1, 6)], []),
    [num2words(10**(3*n), lang="de").split(" ")[-1].lower() for n in range(2, 8)],
    ["hundert", "tausend", "hunderttausend"],
    ["dutzend"]#,
    #["beid", "beide", "einzig"]
], [])

# closing quotation marks
Q_MARKS_C = [u'“', u'«', '"']


# opening quotation marks
Q_MARKS_O = [u'„', u'»', '"']


# quantifying words and their GI tags
QUANTIFIERS = {
	# universal quantifiers
    "all" : "ALL",
    "alle": "ALL",
    #"ganz" : "ALL",
    "gänzlich" : "ALL",
    "immer" : "ALL",
    "immerzu" : "ALL",
    "jed" : "ALL",
    "sämtlich": "ALL",
    "stets" : "ALL",
    "überall" : "ALL",

	# majority quantifiers
    "mehrheitlich" : "MEIST",
    "meist" : "MEIST",
    "meistens" : "MEIST",
    "meistenteils" : "MEIST",
    "überwiegend" : "MEIST",
    "vorwiegend" : "MEIST",

	# vague quantifiers
    "allerlei" : "DIV",
    "einig" : "DIV",
    "einiges" : "DIV",
    "etlich" : "DIV",
    #"etwas" : "DIV",
    "manch" : "DIV",
    "mancherlei" : "DIV",
    "mehrer" : "DIV",
    "viel" : "DIV", 
    "vielerlei" : "DIV", 
    "wenig" : "DIV",
	"zahlreich" : "DIV",

	# vague adverbs
    "bisweilen" : "DIV",
	"einigemal" : "DIV",
	"etlichemal" : "DIV",
    "gängig" : "DIV",
    "gebräuchlich" : "DIV",
    "geläufig" : "DIV",
    #"gern" : "DIV",
    "häufig" : "DIV",
    "herkömmlich" : "DIV",
    "manchmal" : "DIV",
    "mitunter" : "DIV",
    "oft" : "DIV",
	"öfter" : "DIV",
	"öfters" : "DIV",
    "selten" : "DIV",
    "ständig" : "DIV",
    "zeitweise" : "DIV",
	"zuweilen" : "DIV",

	# generic adverbs
	"allgemein" : "DIV",
	"alltäglich" : "DIV",
	"gemeinhin" : "DIV",
	"gemeiniglich" : "DIV",
	"generell" : "DIV",
	"gewöhnlich" : "DIV",
	"gewöhnlicherweise" : "DIV",
	"normalerweise" : "DIV",
	"regelmäßig" : "DIV",
	"typischerweise" : "DIV",
	"üblich" : "DIV",
    "üblicherweise" : "DIV"
}


# semi-modal verbs
SEMI_MODALS = [
    u"drohen", 
    u"lassen", 
    u"pflegen", 
    u"scheinen", 
    u"vermögen", 
    u"versprechen",
    u"wissen"
]


# list of verbs used for direct speech
SPEECH_VERBS = set([
    u'antworten', 
    u'äußern', 
    u'blaffen', 
    u'brüllen', 
    u'denken', 
    u'erwidern', 
    u'erzählen', 
    u'flüstern', 
    u'fragen', 
    u'glauben', 
    u'meinen', 
    u'mutmaßen', 
    u'reden', 
    u'rufen', 
    u'sagen', 
    u'schreien', 
    u'seufzen', 
    u'singen', 
    u'sprechen', 
    u'vermuten', 
    u'weinen', 
    u'wispern'
])


# time adverbs that mark generic VPs
TIME_ADV = [
    "heutzutage"#, "damals"
]

