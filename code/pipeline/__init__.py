import os
import spacy
import sys
from spacy_conll import ConllFormatter
from spacy_sentiws import spaCySentiWS

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from settings import *

# pipeline utensils
import pipeline.annotation
import pipeline.global_constants
import pipeline.global_resources
import pipeline.global_wordlists
import pipeline.passage
import pipeline.utils_classes
import pipeline.utils_methods
from pipeline.global_constants import SPACY_MAX_LENGTH

# pipeline components
from pipeline.components.analyzer import demorphy_analyzer
from pipeline.components.annotation_reader_scene import annotation_reader_scene
from pipeline.components.clausizer import dependency_clausizer, tiger_clausizer, ud_clausizer
from pipeline.components.coref import rb_coref
from pipeline.components.germanet_tagger import germanet_tagger_verbs_adjectives
from pipeline.components.lemma_fixer import lemma_fixer
from pipeline.components.pipeline_pickler import pickle_init, pickle_wrapper
from pipeline.components.scene_tagger import clf_scene_tagger
from pipeline.components.sentencizer import nltk_sentencizer, scene_sentencizer, spacy_sentencizer
from pipeline.components.slicer import max_char_slicer, max_sent_slicer
from pipeline.components.speaker_extractor import rb_speaker_extractor
from pipeline.components.speech_tagger import flair_speech_tagger, quotation_marks_speech_tagger
from pipeline.components.temponym_tagger import heideltime_temponym_tagger
from pipeline.components.tense_tagger import rb_tense_tagger


def pipeline(
    model=os.path.join(PARSING_PATH, "de_ud_lg"), 
    sentencizer="spacy_sentencizer", 
    analyzer="demorphy_analyzer", 
    clausizer="dependency_clausizer", 
    tense_tagger="rb_tense_tagger",
    speech_tagger="quotation_marks_speech_tagger", 
    speaker_extractor="rb_speaker_extractor",
    slicer="max_sent_slicer", 
    coref="rb_coref", 
    temponym_tagger="heideltime_temponym_tagger", 
    germanet_tagger="germanet_tagger_verbs_adjectives",
    scene_tagger="clf_scene_tagger", 
    annotation_reader="annotation_reader_scene",
    max_units=-1,
    disable=[], 
    enable=[], 
    pickle_load=True, 
    pickle_save=True, 
    pickle_overwrite=False, 
    corpus_path=None, 
    _train=None, 
    print_info=True
):
    """Create the spacy pipeline.

    Args:
        model (str): Name/Path of the (German) model to load.
            `de_core_news_[sm|md|lg]` is the pre-trained spacy model for German. The parser uses TIGER dependencies.
            `PARSING_PATH/de_ud_[sm|md|lg]` is the same model as `de_core_news_lg` but with a parser trained on the German UD treebanks.
        sentencizer (str): Name of the `sentencizer` implementation.
        slicer (str): Name of the `slicer` implementation.
        analyzer (str): Name of the `analyzer` implementation.
        clausizer (str): Name of the `clausizer` implementation.
        tense_tagger (str): Name of the `tense_tagger` implementation.
        speech_tagger (str): Name of the `speech_tagger` implementation.
        speaker_extractor (str): Name of the `speaker_extractor` implementation.
        coref (str): Name of the `coref` implementation.
        temponym_tagger (str): Name of the `temponym_tagger` implementation.
        germanet_tagger (str): Name of the `germanet_tagger` implementation.
        scene_tagger (str): Name of the `scene_tagger` implementation.
        annotation_reader ("str"): Name of the `annotation_reader` implementation.
        max_units (int): Maximum number of units (sentences/characters/...) to process; argument for the slicer.
        disable (list of str): List of pipeline components that should be disabled.
        enable (list of str): List of pipeline components that should be enabled.
        pickle_load (boolean): Load intermediate results if they have been calculated before.
        pickle_save (boolean): Save intermediate results.
        pickle_overwrite (boolean): Overwrite intermediate results.
        corpus_path (str): The path to the corpus with annotation collections.
        _train (list of str): List of training documents to train an in-pipeline classifier.
            If None, no classifier is trained.
        print_info (boolean): Iff True, the names of the pipeline components are printed after creating the pipeline.

    Returns:
        `spacy`: The spacy pipeline.
    
    """
    # Load model (includes: 'tagger', 'parser', 'ner')
    sp = spacy.load(model, disable=disable)

    # Increase maximum number of characters per document
    sp.max_length = SPACY_MAX_LENGTH

    # Initalise pickling
    sp.add_pipe(lambda doc: pickle_init(doc, os.path.basename(model), slicer, max_units), name="pickle_init", before="tagger")
    
    # Pickling wrapper
    def pl(doc, func, **kwargs):
        return pickle_wrapper(doc, func, load_output=pickle_load, save_output=pickle_save, overwrite=pickle_overwrite, **kwargs)
    
    # Add lemma fixer
    if enable_component("lemma_fixer", enable, disable):
        sp.add_pipe(lambda doc: pl(doc, lemma_fixer), name="lemma_fixer", after="tagger")
    
    # Add sentencizer
    if enable_component("sentencizer", enable, disable):
        if sentencizer == "nltk_sentencizer":
            sp.add_pipe(lambda doc: pl(doc, nltk_sentencizer), name="sentencizer", before="parser")
        elif sentencizer == "spacy_sentencizer":
            sp.add_pipe(lambda doc: pl(doc, spacy_sentencizer), name="sentencizer", before="parser")
        elif sentencizer == "scene_sentencizer":
            sp.add_pipe(lambda doc: pl(doc, scene_sentencizer, corpus_path=corpus_path), name="sentencizer", before="parser")
        else:
            raise ValueError("Unknown sentencizer: " + sentencizer)
    # (The trained parsers don't have an integrated sentencizer, 
    # i.e. the extra sentencizer component is needed if one of the trained UD models is used.
    # The pre-trained spacy models don't need the extra sentencizer component;
    # it should still be always included to make the sentence splitting independent from the parser.)

    # Add slicer
    if enable_component("slicer", enable, disable):
        if slicer == "max_char_slicer":
            sp.add_pipe(lambda doc: pl(doc, max_char_slicer), name="slicer", after="sentencizer")
        elif slicer == "max_sent_slicer":
            sp.add_pipe(lambda doc: pl(doc, max_sent_slicer), name="slicer", after="sentencizer")
        else:
            raise ValueError("Unknown slicer: " + slicer)

    # Add analyzer
    if enable_component("analyzer", enable, disable):
        if analyzer == "demorphy_analyzer":
            sp.add_pipe(lambda doc: pl(doc, demorphy_analyzer), name="analyzer")
        else:
            raise ValueError("Unknown analyzer: " + analyzer)

    # Add clausizer
    if enable_component("clausizer", enable, disable):
        if clausizer == "dependency_clausizer":
            sp.add_pipe(lambda doc: pl(doc, dependency_clausizer), name="clausizer")
        elif clausizer == "tiger_clausizer":
            sp.add_pipe(lambda doc: pl(doc, tiger_clausizer), name="clausizer")
        elif clausizer == "ud_clausizer":
            sp.add_pipe(lambda doc: pl(doc, ud_clausizer), name="clausizer")
        else:
            raise ValueError("Unknown clausizer: " + clausizer)
    
    # Add tense tagger
    if enable_component("tense_tagger", enable, disable):
        if tense_tagger == "rb_tense_tagger":
            sp.add_pipe(lambda doc: pl(doc, rb_tense_tagger), name="tense_tagger")
        else:
            raise ValueError("Unknown tense_tagger: " + tense_tagger)
    
    # Add speech tagger
    if enable_component("speech_tagger", enable, disable):
        if speech_tagger == "flair_speech_tagger":
            sp.add_pipe(lambda doc: pl(doc, flair_speech_tagger), name="speech_tagger")
        elif speech_tagger == "quotation_marks_speech_tagger":
            sp.add_pipe(lambda doc: pl(doc, quotation_marks_speech_tagger), name="speech_tagger")
        else:
            raise ValueError("Unknown speech_tagger: " + speech_tagger)

    # Add speaker extractor
    if enable_component("speaker_extractor", enable, disable):
        if speaker_extractor == "rb_speaker_extractor":
            sp.add_pipe(lambda doc: pl(doc, rb_speaker_extractor), name="speaker_extractor")
        else:
            raise ValueError("Unknown speaker_extractor: " + speaker_extractor)

    # Add coreference resolution
    if enable_component("coref", enable, disable):
        if coref == "clf_coref":
            sp.add_pipe(lambda doc: pl(doc, clf_coref, corpora_train=_train), name="coref")
        elif coref == "rb_coref":
            sp.add_pipe(lambda doc: pl(doc, rb_coref), name="coref")
        else:
            raise ValueError("Unknown coref: " + coref)
    
    # Add temponym tagger
    if enable_component("temponym_tagger", enable, disable):
        if temponym_tagger == "heideltime_temponym_tagger":
            sp.add_pipe(lambda doc: pl(doc, heideltime_temponym_tagger), name="temponym_tagger")
        else:
            raise ValueError("Unknown temponym_tagger: " + temponym_tagger)
    
    # Add synsets from germanet_tagger
    if enable_component("germanet_tagger", enable, disable):
        if germanet_tagger == "germanet_tagger_verbs_adjectives":
            sp.add_pipe(lambda doc: pl(doc, germanet_tagger_verbs_adjectives), name="germanet_tagger")
        else:
            raise ValueError("Unknown germanet_tagger: " + germanet_tagger) 

    # Add scene segmentation
    if enable_component("scene_tagger", enable, disable):
        if scene_tagger == "clf_scene_tagger":
            sp.add_pipe(lambda doc: pl(doc, clf_scene_tagger, train_dir=_train), name="scene_tagger")
        else:
            raise ValueError("Unknown coref: " + scene_tagger)

    # Add annotation reader:
    if enable_component("annotation_reader", enable, disable):
        if annotation_reader == "annotation_reader_scene":
            sp.add_pipe(lambda doc: annotation_reader_scene(doc, corpus_path=corpus_path), name="annotation_reader")
        else:
            raise ValueError("Unknown annotation_reader: " + annotation_reader)

    if print_info:
        print(sp.pipe_names)
    
    return sp


def enable_component(name, enable, disable):
    """Check whether a component should be enabled.

    Args:
        name (str): The name of the component.
        enable (list of str): Names of components to enable.
        disable (list of str): Names of components to disable.
    
    Returns:
        boolean: True iff the component should be enabled.
    
    """
    if len(enable) > 0 and len(disable) > 0:
        ValueError("You can either enable or disable components but not both!")
    if len(enable) == 0 and len(disable) == 0:
        return True
    if (len(enable) > 0 and name in enable) or (len(disable) > 0 and name not in disable):
        return True
    return False
