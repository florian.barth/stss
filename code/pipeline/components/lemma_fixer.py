def lemma_fixer(doc):
    """Spacy pipeline component.
        Fix wrong lemmas from the built-in spacy lemmatizer.
        Spacy basically lemmatises using this list:
            https://raw.githubusercontent.com/explosion/spacy-lookups-data/master/spacy_lookups_data/data/de_lemma_lookup.json

    Args:
        doc (`Doc`): A spacy document object.
    
    Returns:
        `Doc`: A spacy document object.

    """
    # List of words to be re-lemmatised
    # (the idea is to add frequent words here to keep the list short;
    # not every uncommon word spacy has trouble with):
    exceptions = {
        # lowercased word : { POS tag : new lemma }
        "muß" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "mußtest" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "sein" : dict.fromkeys(["AUX", "VERB"], "sein"),
        "soll" : dict.fromkeys(["AUX", "VERB"], "sollen"),
        "wolle" : dict.fromkeys(["AUX", "VERB"], "wollen")
    }
    for token in doc:
        if token.lower_ in exceptions and token.pos_ in exceptions[token.lower_]:
            token.lemma_ = exceptions[token.lower_][token.pos_]
    return doc