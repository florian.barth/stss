import spacy
from spacy.tokens import Doc, Span

from ..classifiers.clf_scene_tagger import get_clf_scene_tagger, extract_cluster_infos, extract_last_mentions
from ..passage import Passage
from ..utils_methods import add_extension, get_doc_text


def clf_scene_tagger(doc, train_dir=None):
    """Spacy pipeline component.
        Add scenes to a document using a statistical classifier.

    Args:
        doc (`Doc`): A spacy document object.
        texts_dir (str): Directory to training texts.
    
    Returns:
        `Doc`: A spacy document object.

    """
    clf_predict = get_clf_scene_tagger(train_dir, cv_exclude=get_doc_text(doc))

    add_extension(Doc, "scenes")
    add_extension(Span, "scene")
    add_extension(Span, "scene_tag")
    add_extension(Span, "scene_transition")

    scenes_sents = []
    scenes_tags = []
    cluster_infos = extract_cluster_infos(doc)
    last_mentions = extract_last_mentions(doc)
    sents = list(doc.sents)
    for i, sent in enumerate(sents):
        tags = clf_predict(sents, i, cluster_infos, last_mentions)
        if len(tags) == 0:
            if len(scenes_sents) > 0:
                scenes_sents[-1].append(sent)
            else:
                scenes_sents.append([sent])
                scenes_tags.append(set(["Scene-to-Nonscene"]))
        else:
            scenes_sents.append([sent])
            scenes_tags.append(tags)

    doc._.scenes = []
    for n, scene_sents in enumerate(scenes_sents):
        scene = doc[scene_sents[0].start:scene_sents[-1].end]
        scene._.scene_transition = scenes_tags[n].pop()
        scene._.scene_tag = scene._.scene_transition.split("-")[-1]
        doc._.scenes.append(scene)
        for sent in scene_sents:
            sent._.scene = scene
    
    return doc