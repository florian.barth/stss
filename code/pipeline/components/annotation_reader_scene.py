import json
import os
import sys

from spacy.tokens import Doc, Span, Token
from ..annotation import Annotation, AnnotationList
from ..utils_methods import add_extension, get_doc_text, get_sub_clauses, map_tokens_to_closest_clauses

def find_json_path(doc, corpus_path):
    """Find the json file for a document.

    Args:
        doc (`Doc`): The document.
        corpus_path (str): The path to the corpus.
            The corpus is a directory of JSON files.
    
    Returns:
        str: The path to the directory with the annotations.
            Returns None if no directory is found.
    
    """
    text = get_doc_text(doc, normalized=False)
    for file in os.listdir(corpus_path):
        file = os.path.join(corpus_path, file)
        if file.endswith(".json"):
            with open(file) as f:
                data = json.load(f)
                if data["text"] == text:
                    return file
    return None


def annotation_reader_scene(doc, corpus_path):
    """Find and append annotations of a scene annotation dict to a spacy doc, it's clauses and tokens.

    Args:
        doc (`Doc`): The document.
        json_dict (str): dict of json file
    
    Returns:
        doc (`Doc`): A spacy document object.
    
    """
    add_extension(Doc, "annotations")
    add_extension(Span, "annotations")
    add_extension(Token, "annotations")

    json_path = find_json_path(doc, corpus_path)
    if json_path is None:
        return doc
    
    with open(json_path, 'r') as f:
        json_dict = json.load(f)

    from .. import pipeline
    pipe = pipeline(pickle_load=False, pickle_save=False, enable=["sentencizer", "clausizer"], print_info=False)
    
    sub_clauses, sub_clause_starts, sub_clause_ends = get_sub_clauses(doc)

    # define empty AnnotationList for doc
    doc._.annotations = dict()
    doc._.annotations[""] = AnnotationList()

    # 1. assign tokens to character positions for faster look-up
    # 2. assign empty AnnotationLists to every token for every annotator
    charpos_to_token_i = {}
    for token_i, token in enumerate(doc):
        try:
            charpos = token._.idx
        except AttributeError:
            charpos = token.idx
        charpos_to_token_i[charpos] = token_i

        token._.annotations = dict()
        token._.annotations[""] = AnnotationList()

    # do the same for clauses, if the document is clausized
    if hasattr(doc._, "clauses"):
        for clause in doc._.clauses:
            clause._.annotations = dict()
            clause._.annotations[""] = AnnotationList()


    for annotation in json_dict["scenes"]:
        token_indices = list()
        anno_range = range(annotation["begin"], annotation["end"])
        for charpos in anno_range:
            try:
                token_indices.append(charpos_to_token_i[charpos])
            except KeyError:
                pass

        # define span as list of tokens
        anno_span = [doc[token_index] for token_index in token_indices]
        # add Annotation-object to anno_span._.annotation
        # add Annotation-object to anno_span._.scene_annotation
        annotation_object = Annotation(tag=annotation['type'],
                                    tokens=anno_span,
                                    clauses=[],
                                    tagset=None,
                                    property_values_dict=None,
                                    id_value=None,
                                    strings=[json_dict["text"][annotation["begin"]:annotation["end"]]],
                                    string_positions=[(annotation["begin"],annotation["end"])]                                   
                                    )

        if len(sub_clauses) > 0:
            annotation_object.clauses = map_tokens_to_closest_clauses(anno_span, sub_clauses, sub_clause_starts, sub_clause_ends)

        doc._.annotations[""].append(annotation_object)

        # append AnnotationList to corresponding tokens
        for token_of_span in anno_span:
            token_of_span._.annotations[""].append(annotation_object)

        # append AnnotationList to corresponding clauses
        for clause in annotation_object.clauses:
            clause._.annotations[""].append(annotation_object)

    return doc