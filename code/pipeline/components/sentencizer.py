import json
import os
import spacy
from nltk.tokenize import sent_tokenize

from ..global_resources import SPACY_MODEL
from ..utils_methods import get_doc_text

def nltk_sentencizer(doc):
    """Spacy pipeline component.
        Splits the document into sentences using the `sent_tokenize` from NLTK.

    Args:
        doc (`Doc`): A spacy document object.
    
    Returns:
        `Doc`: A spacy document object.

    """
    sents = sent_tokenize(doc.text, language="german")
    return align_sents(doc, sents)


def spacy_sentencizer(doc):
    """Spacy pipeline component.
        Splits the document into sentences using the `de_core_news_lg` model from spacy.

    Args:
        doc (`Doc`): A spacy document object.
    
    Returns:
        `Doc`: A spacy document object.

    """
    global SPACY_MODEL
    SPACY_MODEL.provide()
    
    sents = [sent.text for sent in SPACY_MODEL._(doc.text).sents]
    return align_sents(doc, sents)


def scene_sentencizer(doc, corpus_path):
    """Spacy pipeline component.
        Splits the document into sentences using the pre-split files in `corpus_path`.
        The files must have the same format as the files for the Shared Task on Scene Segmentation (http://lsx-events.informatik.uni-wuerzburg.de/stss-2021/).

    Args:
        doc (`Doc`): A spacy document object.
    
    Returns:
        `Doc`: A spacy document object.

    """
    text = get_doc_text(doc, normalized=False)
    for file in os.listdir(corpus_path):
        file = os.path.join(corpus_path, file)
        if file.endswith(".json"):
            with open(file) as f:
                data = json.load(f)
                if data["text"] == text:
                    sent_starts = [int(d["begin"]) for d in data["sentences"]]
                    for token in doc:
                        token.is_sent_start = False
                        charpos = token._.idx if hasattr(token._, "idx") else token.idx
                        if len(sent_starts) > 0 and charpos >= sent_starts[0]:
                            token.is_sent_start = True
                            sent_starts = sent_starts[1:]
                    return doc
    raise FileNotFoundError("File not found in " + corpus_path)


def align_sents(doc, sents):
    """Aligns the output of a sentence splitter with a spacy document.
        Iterates over every token and sets the `is_sent_start` property.
    
    Args:
        doc (`Doc`): A spacy document object.
        sents (list of str): The output of an external sentence splitter applied to the document's text.
    
    Returns:
        `Doc`: A spacy document object.
    
    """
    if len(sents) == 0:
        return doc
    current_sent_tool = sents.pop(0)
    current_sent_spacy = ""
    for token in doc:
        if current_sent_tool.strip() == current_sent_spacy.strip():
            token.is_sent_start = True
            if len(sents) == 0:
                return doc
            current_sent_tool = sents.pop(0)
            current_sent_spacy = ""
        else:
            token.is_sent_start = False
        current_sent_spacy += token.text_with_ws
    if len(doc) > 0:
        doc[0].is_sent_start = True
    return doc