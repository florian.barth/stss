from spacy.tokens import Span

from .utils_classes import NonEmptyList, NonEmptySet


class Passage():
    """A class for passages, i.e. sequences of clauses.

    """
    def __init__(self, clauses, tags):
        """__init__ method of the class `Passage`.

        Args:
            clauses (list of `Span`): List of clauses belonging to the passage.
            tags (set of str): Tags assigned to the passage.
        
        """
        if not (((isinstance(clauses, list) and len(clauses) > 0) or isinstance(clauses, NonEmptyList)) and isinstance(clauses[0], Span)):
            raise ValueError("Passages must contain a non-empty list of clauses")
        self.clauses = NonEmptyList(clauses)
        if not (((isinstance(tags, set) and len(tags) > 0) or isinstance(tags, NonEmptySet)) and isinstance(list(tags)[0], str)):
            raise ValueError("Passages must contain a non-empty set of tags")
        self.tags = NonEmptySet(tags)
        self.tokens = sorted([token for clause in self.clauses for token in clause._.tokens], key=lambda token: token.i)
        self.text = " ".join([token.text for token in self.tokens])
    
    def stringify(self):
        """String representation of the class `Passage`.
        
        Returns:
            str: Joined list of the stringified tokens of the passage's clauses.
        
        """
        return self.text + " # " + ",".join(sorted(list(self.tags)))
    
    def __str__(self):
        """__str__ method of the class `Passage`.

        Returns:
            str: Joined list of the stringified tokens of the passage's clauses.
        
        """
        return self.stringify()
    
    def __repr__(self):
        """__repr__ method of the class `Passage`.

        Returns:
            str: Joined list of the stringified tokens of the passage's clauses.
        
        """
        return self.stringify()