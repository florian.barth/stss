"""This file contains useful classes for developing the SpaCy pipeline.
"""

class Debugger():
    """A simple class for debugging.
    
    """
    def __init__(self, debug=False):
        self.DEBUG = debug

    def print(self, *values):
        if self.DEBUG:
            print(*values)

    def input(self):
        if self.DEBUG:
            input()


class Form():
    """A class that can store surface-syntactical analysis information of a clause.
        To access an attribute of a clause, e.g. tense, use:
            clause._.form.tense

    """    
    def __init__(self):
        """__init__ method of the class `Form`.

        """
        self._feats = [
            "tense", # 'pres', 'past', 'fut'
            "aspect", # 'imperf', 'perf'
            "mode", # 'imp', 'ind', 'subj:pres', 'subj:past'
            "voice", # 'active', 'pass', 'pass:dynamic', 'pass:static'
            "verb_form", # 'fin', 'inf', 'part'
            "main", # (main verb `Token`)
            "modals", # (list of modal verbs `Token`)
            "verbs" # (list of verbs `Token`)
        ]
        
        # add features as attributes
        for feat in self._feats:
            setattr(self, feat, None)
        
        # change default value for verb lists from None to empty list
        self.main = None
        self.modals = []
        self.verbs = []

    
    def as_dict(self):
        """Convert the object's attributes to a dictionary.
        
        Returns:
            dict of str:(set of str): Dictionary with syntactical features and the corresponding values.
        
        """
        as_dict = vars(self)
        as_dict = {key : as_dict[key] for key in self._feats}
        return as_dict


    def __str__(self):
        """__str__ method of the class `Form`.

        Returns:
            str: String representation of the object's attributes.
                Only shows used attributes.
        
        """
        return str(self.as_dict())


class Morph():
    """A class that can store morphological analysis information of a token.
        To access an attribute of a token, e.g. tense, use:
            token._.morph.tense_
            token._.morph.tense
        The first expression returns all possible tenses of the token in a set (the set can be empty).
        The second expression resturns the tense of the token if it is existing and unambiguous; otherwise it returns None.

    """    
    def __init__(self, spacy_features={}, demorphy_features={}):
        """__init__ method of the class `Morph`.

        Args:
            spacy_features (dict of str:(set of str)): Morphological features of a spacy token.
                The features of a token can be accessed through TAG_MAP[token.tag_] (but have be converted to the desired format).
            demorphy_features (dict of str:(set of str)): Morphological features of a demorphy analysis.
                The features of a token can be accessed through Analyzer().analyze(token.text) (but have be converted to the desired format).
        
        """
        # (all) features from spacy and possible values:
        self._spacy_feats = [
            'AdpType', # 'circ', 'prep', 'post'
            'Aspect', # 'perf'
            'ConjType', # 'comp'
            'Foreign', # 'yes'
            'Hyph', # 'yes'
            'Mood', # 'ind', 'imp'
            'NumType', # 'card'
            'PartType', # 'inf', 'res', 'vbp'
            'Polarity', # 'neg'
            'Poss', # 'yes'
            'PronType', # 'dem', 'ind|neg|tot', 'prs', 'rel', 'int', 'art'
            'PunctType', # 'peri', 'comm', 'brck'
            'Reflex', # 'yes'
            'VerbForm', # 'fin', 'inf', 'part'
            'VerbType' # 'mod'
        ]

        # (some) features from demorphy and possible values:
        self._demorphy_feats = [
            "CASE", # "nom", "acc", "dat", "gen"
            "PERSON", # "1per", "2per", "3per"
            "NUMERUS", # "sing", plu
            "GENDER", # "masc", "fem", "neut", "noGender"
            "TENSE", # "pres", "ppres", "past", "ppast"
            "MODE", # "imp", "ind", "subj"
            "INFLECTION", # "inf", "zu"
            "DEGREE", # "pos", "comp", "sup"
            "STARKE", # "strong", "weak"
            "ADDITIONAL_ATTRIBUTES" # "<mod>", "<aux>", "<adv>", "<pred>", "<ans>", "<attr>", "<adj>", "<cmp>", "<coord>", "<def>", "<indef>", "<noinfl>", "<neg>", "personal", "<prfl>", "<rec>", "<pro>", "<refl>", "<subord>", "<subst>"
        ]

        # add both feature sets as attributes
        # (attribute names are lowercased and camelcases are changed to underscores):
        for feat in self._spacy_feats + self._demorphy_feats:
            feat = feat.replace("Type", "_Type").replace("Form", "_Form").lower()
            setattr(self, feat, None)
            setattr(self, feat + "_", set())
        
        # set the attributes' values:
        self._integrate_values(spacy_features)
        self._integrate_values(demorphy_features)

    
    def _integrate_values(self, dd):
        """Method to fill feature-values pairs into the object's attributes.

        Args:
            dd (dict of str:(set of str)): feature-values pairs
        
        """
        for key in dd.keys():
            val = dd[key]
            try:
                key = key.replace("Type", "_Type").replace("Form", "_Form").lower()
                if hasattr(self, key):
                    if len(val) == 1:
                        setattr(self, key, list(val)[0])
                    setattr(self, key + "_", val)
            except:
                pass


    def as_dict(self):
        """Convert the object's attributes to a dictionary.
            Only keep those attributes which have at least one value.
        
        Returns:
            dict of str:(set of str): Dictionary with morphological features and the corresponding values.
        
        """
        as_dict = vars(self)
        as_dict = {key : as_dict[key] for key in as_dict if key.endswith("_") and len(as_dict[key]) > 0}
        return as_dict
    
    
    def __str__(self):
        """__str__ method of the class `Morph`.

        Returns:
            str: String representation of the object's attributes.
                Only shows used attributes.
        
        """
        return str(self.as_dict())


class NonEmptyList(list):
    """A list that must not be empty.

    """
    def __init__(self, items):
        super(NonEmptyList, self).__init__(items)
        self.empty_check()
    def clear(self):
        super(NonEmptyList, self).clear()
        self.empty_check()
    def pop(self, index):
        res = super(NonEmptyList, self).pop(index)
        self.empty_check()
        return res
    def remove(self, item):
        super(NonEmptyList, self).remove(item)
        self.empty_check()
    def empty_check(self):
        if len(self) == 0:
            raise ValueError("List must not be empty")


class NonEmptySet(set):
    """A set that must not be empty.

    """
    def __init__(self, items):
        super(NonEmptySet, self).__init__(items)
        self.empty_check()
    def clear(self):
        super(NonEmptySet, self).clear()
        self.empty_check()
    def difference(self, set_):
        res = super(NonEmptySet, self).difference(set_)
        self.empty_check()
        return res
    def difference_update(self, set_):
        super(NonEmptySet, self).difference_update(set_)
        self.empty_check()
    def discard(self, item):
        super(NonEmptySet, self).discard(item)
        self.empty_check()
    def intersection(self, set_):
        res = super(NonEmptySet, self).intersection(set_)
        self.empty_check()
        return res
    def intersection_update(self, set_):
        super(NonEmptySet, self).intersection_update(set_)
        self.empty_check()
    def pop(self):
        res =  super(NonEmptySet, self).pop()
        self.empty_check()
        return res
    def remove(self, item):
        super(NonEmptySet, self).remove(item)
        self.empty_check()
    def symmetric_difference(self, set_):
        res = super(NonEmptySet, self).symmetric_difference(set_)
        self.empty_check()
        return res
    def symmetric_difference_update(self, set_):
        super(NonEmptySet, self).symmetric_difference_update(set_)
        self.empty_check()
    def empty_check(self):
        if len(self) == 0:
            raise ValueError("Set must not be empty")
