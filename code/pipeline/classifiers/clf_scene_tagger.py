import json
import numbers
import numpy as np
import os
import pickle
import re
import tensorflow as tf
from datetime import datetime
import matplotlib.pyplot as plt
from nltk import ngrams
from numpy.lib.function_base import average
from sklearn import tree
from sklearn.feature_extraction import DictVectorizer
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import StandardScaler #for SVC, LogisticRegression, KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.feature_selection import f_classif, SelectKBest, VarianceThreshold
from sklearn.metrics import f1_score, make_scorer, precision_score, recall_score, accuracy_score
from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression
from sklearn.naive_bayes import MultinomialNB
from sklearn.neighbors import KNeighborsClassifier
from tensorflow.keras import models, layers, optimizers, regularizers
from tensorflow.keras.utils import to_categorical

from ..global_resources import SPACE_NOUNS, GERMANET
from ..utils_methods import *
import sys
sys.path.append("../..")
from settings import *

# labels and encoder for single-label classification
SCENE_TAGS = ["Scene", "Nonscene"]
EXTENDED_SCENE_TAGS = ["Scene-to-Scene", "Scene-to-Nonscene", "Nonscene-to-Scene"]
LABEL_ENCODER = LabelEncoder()
LABEL_ENCODER.fit(EXTENDED_SCENE_TAGS + ["None"])

# Name of a pre-trained classifier
DEFAULT_CLF_NAME = "scene_tagger_RandomForestClassifier_T11_MSL3"


def get_clf_scene_tagger(train_dir=None, cv_exclude=None, classifier=RandomForestClassifier, plot_tree_to="scene_tree.png", pickle_vecs=True, subselect_features_k=None, pickle_clfs=False):
    """Returns a function that maps a sample to a label using a classifier.

    Args:
        texts_train (list of str): List of training texts.
            If None, the default classifier is loaded.
        cv_exclude (str): The document (as plain text) to exclude from the training set. (Used for cross-validation.)
            If None, no text is excluded.
        classifier (class): A classifier, e.g. `RandomForestClassifier`.
        plot_tree_to (str): For tree classifiers, plot the (first) decision tree to a file at the given path.
            If None, nothing is plotted.
        pickle_vecs (boolean): Iff True, vectorised training samples are dumped and loaded with pickle.
        pickle_clfs (boolean): Iff True, classifiers are dumped and loaded with pickle.
        subselect_features_k (int): Maximum number of best features to select from all featurs.
            If None, no feature selection is applied.
    
    Returns:
        func: A function that maps a clause (more precisely: a list of clauses and a clause index) to a GI label.
    
    """
    current_dir = os.path.dirname(__file__)
    if train_dir is None:
        # load the default classifier
        clf_path = os.path.join(current_dir, "pickled_clfs", DEFAULT_CLF_NAME + ".pickle")
        clf, dv, fs1, fs2 = pickle.load(open(clf_path, "rb"))
    else:
        clf_name = "scene_tagger_" + classifier.__name__
        clf_path = os.path.join(current_dir, "pickled_clfs", clf_name + ".pickle")
        if os.path.exists(clf_path) and pickle_clfs:
        # load the classifier trained on the given documents
            clf, dv, fs1, fs2 = pickle.load(open(clf_path, "rb"))
        else:
            # train the classifier on the given documents and save it
            from .. import pipeline
            pipe = pipeline(
                enable=["sentencizer", "analyzer", "clausizer", "tense_tagger", "speech_tagger", "speaker_extractor", "coref", "temponym_tagger", "annotation_reader"], 
                sentencizer="scene_sentencizer", 
                annotation_reader="annotation_reader_scene", 
                corpus_path=train_dir, 
                print_info=False
            )
            X_train, Y_train = [], []
            for text_train in os.listdir(train_dir):
                data = json.load(open(os.path.join(train_dir, text_train), "r"))
                if data["text"] == cv_exclude:
                    print(text_train, "excluded for cv")
                    continue
                print(text_train)
                vec_path = os.path.join(current_dir, "pickled_vecs", "stss", "scene_tagger_" + text_train + ".pickle")
                if pickle_vecs and os.path.exists(vec_path):
                    # load the vectors for the given document
                    X, Y = pickle.load(open(vec_path, "rb"))
                else:
                    # compute the vectors for the given document and save them
                    doc_train = pipe(data["text"])
                    X, Y = vectorize_doc(doc_train)
                    if pickle_vecs:
                        pickle.dump((X, Y), open(vec_path, "wb"))
                
                # construct training samples of several subsequent sentences each
                X_with_context = []
                for index, x in enumerate(X):
                    x_with_context = {}
                    for i in [-2, -1, 0, 1, 2]:
                        try:
                            x_ = X[index+i]
                            for k in x_:
                                x_with_context["sent_"+str(i)+"_"+k] = x_[k]
                        except IndexError:
                            pass
                    X_with_context.append(x_with_context)
                X = X_with_context

                X_train.extend(X)
                Y_train.extend(Y)
            
            feat_count = {}
            for x in X_train:
                for k in x:
                    try:
                        feat_count[k] += 1
                    except KeyError:
                        feat_count[k] = 1
            X_train = [{k : x[k] for k in x if feat_count[k] >= 5} for x in X_train]
            
            # filtering features
            #X_train = [{k : x[k] for k in x if ("dist" not in k)} for x in X_train]
            #X_train = [{k : x[k] for k in x if ("temponym_substr" not in k)} for x in X_train]
            #X_train = [{k : x[k] for k in x if ("germanet_verb_category" not in k)} for x in X_train]
            #X_train = [{k : x[k] for k in x if ("index" not in k)} for x in X_train]
            X_train = [{k : x[k] for k in x if ("mention_dep" not in k or x[k] in ["nsubj", "obj", "obl", "nmod"])} for x in X_train]

            # encode features
            dv = DictVectorizer(sparse=True)
            dv.fit(X_train)
            X_train = dv.transform(X_train)

            if classifier in [SVC]:
                X_train[X_train < 0] = 1000000

            # convert tagset ["Scene", "Nonscene", "None"] to 
            # ["Scene-to-Scene", "Scene-to-Nonscene", "Nonscene-to-Scene", "None"]
            # (the first sentence is either "Scene-to-Scene" or "Scene-to-Nonscene")
            Y_train_2 = []
            current = "Scene"
            for i in range(len(Y_train)):
                if Y_train[i] in ["Scene", "Nonscene"]:
                    Y_train_2.append(current+"-to-"+Y_train[i])
                    current = Y_train[i]
                else:
                    Y_train_2.append(Y_train[i])
            Y_train = Y_train_2

            # encode labels (single-label classification)
            Y_train = LABEL_ENCODER.transform(Y_train)
            Y_train = list(Y_train)
            #Y_train = to_categorical(Y_train)

            # standard scaler for KNN
            if classifier in [KNeighborsClassifier, SVC, LogisticRegression]:
                print("process StandardScaler")
                scaler = StandardScaler(with_mean=False)
                scaler.fit(X_train)
                X_train = scaler.transform(X_train)

            print("feats:", X_train.shape)
            
            fs1 = VarianceThreshold(threshold=0)
            fs1.fit(X_train, Y_train)
            X_train = fs1.transform(X_train)
            print("feats:", X_train.shape)
                
            fs2 = None
            if subselect_features_k is not None:
                fs2 = SelectKBest(f_classif, k=subselect_features_k)
                fs2.fit(X_train, Y_train)
                X_train = fs2.transform(X_train)
                print("feats:", X_train.shape)
            
            if False:
                # rank features
                fsx = SelectKBest(f_classif, k="all")
                fsx.fit(X_train, Y_train)
                print("feature weights")
                for s, n in sorted(zip(fsx.scores_, dv.feature_names_), reverse=True)[:200]:
                    try:
                        c = feat_count[n]
                    except:
                        try:
                            c = feat_count["=".join(n.split("=")[:-1])]
                        except:
                            c = -1
                    print(s, n, c)
                print()

            clf = classifier(criterion="entropy", class_weight="balanced", random_state=0, max_depth=11, min_samples_leaf=3)
            #clf = AdaBoostClassifier(base_estimator=clf, n_estimators=50, random_state=0)

            clf.fit(X_train, Y_train)
            classifier_str = re.search("'(.*)'", str(classifier)).group(1)
            print("classifier: ", classifier_str)
            print("classifier params: " + str(vars(clf)) )
            print("subselect_features_k=", str(subselect_features_k))
            #for y in clf.predict(X_train):
            #    print(y, LABEL_ENCODER.inverse_transform([y]))
            #    input()
            print("fscore train data", f1_score(Y_train, clf.predict(X_train), average="macro", labels=LABEL_ENCODER.transform(EXTENDED_SCENE_TAGS)))
            if pickle_clfs:
                pickle.dump((clf, dv, fs1, fs2), open(clf_path, "wb"))

    if plot_tree_to is not None and classifier in [DecisionTreeClassifier, RandomForestClassifier]:
        if type(clf) == DecisionTreeClassifier:
            clf_tree = clf
        else:
            clf_tree = clf.estimators_[0]
        plt.figure(figsize=(120,40))
        if subselect_features_k is None:
            tree.plot_tree(clf_tree, max_depth=10, feature_names=dv.feature_names_, class_names=[str(c) for c in LABEL_ENCODER.classes_], fontsize=10)
        else:
            tree.plot_tree(clf_tree, max_depth=10, feature_names=fs2.transform(fs1.transform([dv.feature_names_]))[0], class_names=[str(c) for c in LABEL_ENCODER.classes_], fontsize=10)
        plt.savefig(plot_tree_to)
    
    
    def clf_predict(sents, i, cluster_infos, last_mentions):
        x = feature_extractor(sents, i, cluster_infos, last_mentions)
        X = [x]
        X = dv.transform(X)
        if fs1 is not None:
            X = fs1.transform(X)
        if fs2 is not None:
            X = fs2.transform(X)
        Y = clf.predict(X)
        Y = LABEL_ENCODER.inverse_transform(Y)
        y = Y[0]
        tags = set([y])
        tags.discard("None")
        return tags
    
    return clf_predict


def vectorize_doc(doc):
    """Vectorize the sentences of a document.

    Args:
        doc (`Doc`): The document.
    
    Returns:
        list of (dict of str:str): A list of feature dicts (feature-value pairs), one for every sentence.
        list of str: A list of corresponding gold classes (single labels); `None` for non-scene-initial sentences.
    
    """
    X = []
    Y = []
    annotations = doc._.annotations[""].get_annotations(tags=SCENE_TAGS)
    annotations = [annotation for annotation in annotations if len(annotation.tokens) > 1]
    cluster_infos = extract_cluster_infos(doc)
    last_mentions = extract_last_mentions(doc)
    sents = list(doc.sents)
    for i, sent in enumerate(sents):
        features = feature_extractor_(sents, i, cluster_infos, last_mentions)
        label = "None"
        if len(annotations) > 0 and annotations[0].tokens[0] in sent:
            label = annotations[0].tag
            annotations = annotations[1:]
        X.append(features)
        Y.append(label)
    return X, Y


def extract_cluster_infos(doc):
    """Extract information about coreference clusters in a document.

    Args:
        doc (`Doc`): The document.
    
    Returns:
        dict of str:obj: Feature dict.
    
    """
    cluster_infos = {}
    for cluster in doc._.coref_clusters:
        info = {}
        mention_types = [mention.lemma_ for mention in cluster.mentions if mention.root.pos_ in ["NOUN", "PROPN"]]
        mention_type_counts = sorted([mention_types.count(mention_type) for mention_type in set(mention_types)])
        info["cluster_len"] = len(cluster.mentions)
        try:
            info["cluster_ratio"] = 1.0*mention_type_counts[-2]/mention_type_counts[-1]
        except IndexError:
            info["cluster_ratio"] = 0.0
        info["cluster_is_pron"] = 1.0*len([mention for mention in cluster.mentions if is_PRON(mention)])/len(cluster.mentions)
        info["cluster_is_ne"] = 1.0*len([mention for mention in cluster.mentions if is_NE(mention)])/len(cluster.mentions)
        info["cluster_is_nn"] = 1-info["cluster_is_pron"]-info["cluster_is_ne"]
        cluster_infos[cluster.i] = info
    return cluster_infos


def extract_last_mentions(doc):
    """Extract for each mention in a document when the corresponding entity was last mentioned.
        Mentions inside direct speech are ignored.
    
    Args:
        doc (`Doc`): The document.
    
    Returns:
        dict of int:int: A dictionary mapping the index of a mention's root token to the index of its preceding mention's root token.
    """
    last_mentions = {}
    space_mentions = []
    for cluster in doc._.coref_clusters:
        mentions = []
        for mention in cluster.mentions:
            if not is_direct_speech(mention):
                if is_space_ent(mention):
                    space_mentions.append(mention)
                else:
                    mentions.append(mention)
        if len(mentions) > 0:
            last_mention = mentions[0]
            for current_mention in mentions[1:]:
                last_mentions[current_mention.root.i] = last_mention.root.i
                last_mention = current_mention
    space_mentions = sorted(space_mentions, key=lambda mention: mention.start-1.0/(mention.end-mention.start))
    for i in range(len(space_mentions), 0, -1):
        current_mention = space_mentions[i-1]
        _, nouns1, _ = get_head_nouns(current_mention, form="lemma_")
        for j in range(i-1, 0, -1):
            last_mention = space_mentions[j-1]
            _, nouns2, _ = get_head_nouns(last_mention, form="lemma_")
            if len(set(nouns1).intersection(set(nouns2))) > 0:
                last_mentions[current_mention.root.i] = last_mention.root.i
                break
    return last_mentions


def feature_extractor(sents, index, cluster_infos, last_mentions):
    """Extract features from a sentence and its preceding and succeeding sentences.

    Args:
        sents (list of `Span`): List of sentences in the document.
        index (int): Index of the current sentence.
        cluster_infos (dict of str:obj): Output of `extract_cluster_infos`.
        last_mentions (dict of int:int): Output of `extract_last_mentions`.
    
    Returns:
        dict of str:obj: Feature dict.
    
    """
    features = {}
    for i in [-2, -1, 0, 1, 2]:
        features_ = feature_extractor_(sents, index+i, cluster_infos, last_mentions)
        for k in features_:
            features["sent_"+str(i)+"_"+k] = features_[k]
    return features


def feature_extractor_(sents, index, cluster_infos, last_mentions, include_sent_features=True, include_change_features=True, include_ngram_features=False):
    """Extract features from a sentence.

    Args:
        sents (list of `Span`): List of sentences in the document.
        index (int): Index of the current sentence.
        cluster_infos (dict of str:obj): Output of `extract_cluster_infos`.
        last_mentions (dict of int:int): Output of `extract_last_mentions`.
        include_sent_features (boolean): True iff the features from `extract_sent_features` should be extracted.
        include_change_features (boolean): True iff the features from `extract_change_features` should be extracted.
        include_ngram_features (boolean): True iff the features from `extract_ngram_features` should be extracted.
    
    Returns:
        dict of str:obj: Feature dict.
    
    """
    features = {}
    all_features = []
    if include_sent_features:
        all_features.append(extract_sent_features(sents, index))
    if include_change_features:
        all_features.append(extract_change_features(sents, index, cluster_infos, last_mentions))
    if include_ngram_features:
        all_features.append(extract_ngram_features(sents, index))
    for features_ in all_features:
        for k in features_:
            features[k] = features_[k]
    return features


def extract_sent_features(sents, index):
    """Extract sentence features.

    Args:
        sents (list of `Span`): List of sentences in the document.
        index (int): Index of the current sentence.
    
    Returns:
        dict of str:obj: Feature dict.
    
    """
    features = {}
    try:
        sent = sents[index]
        features["index_start"] = min(index,10)
        features["index_end"] = min(len(sents)-1-index,10)
        GERMANET.provide()
        for clause in sent._.clauses:
            if clause.root.dep_.lower() == "root": # matrix clause
                # grammatical features
                features["tense"] = clause._.form.tense
                features["aspect"] = clause._.form.aspect
                features["mode"] = clause._.form.mode
                features["voice"] = clause._.form.voice
                features["verb_form"] = clause._.form.verb_form
                for verb in clause._.form.modals:
                    features["modal:" + verb.lemma_.lower()] = True
                # direct speech
                if is_direct_speech(clause):
                    features["direct_speech"] = True
                # germanet verb categories
                if clause._.verb_synset_id is not None:
                    features["germanet_verb_category:" + str(GERMANET._.get_synset_by_id(clause._.verb_synset_id).word_class)] = True
                # verbs
                for token in clause._.tokens:
                    if token.pos_ in ["VERB", "AUX"]:
                        features["verb_pos:" + token.pos_] = True
            else: # subordinate clauses
                # direct speech
                if is_direct_speech(clause):
                    features["direct_speech_"] = True
                # germanet verb categories
                if clause._.verb_synset_id is not None:
                    features["germanet_verb_category_:" + str(GERMANET._.get_synset_by_id(clause._.verb_synset_id).word_class)] = True
                # head relations
                features["dep_:" + clause.root.dep_.split(":")[0]] = True
        is_first = True
        token_last = None
        for token_i, token in enumerate(sent):
            # punctuation
            if token.is_punct:
                features["punct:" + token.text] = True
            # discourse connectives
            if is_first and not (token.is_punct or token.is_space):
                features["first_pos"] = token.pos_
                features["first_dep"] = token.dep_.split(":")[0]
                #features["first_text"] = token.text.lower()
                is_first = False
            # space
            if token.is_space:
                if len(sent) == 1:
                    features["space_first"] = True
                    features["space_last"] = True
                elif token_i == 0:
                    features["space_first"] = True
                elif token_i == len(sent)-1:
                    features["space_last"] = True
                else:
                    features["space_mid"] = True
            else:
                token_last = token
        # final punctuation
        if token_last is not None and token_last.is_punct:
            features["punct_last:" + token.text] = True
        # sentence features
        features["sent_len_tokens"] = len(sent)
        features["sent_len_clauses"] = len(sent._.clauses)
    except IndexError:
        pass
    return features


def extract_change_features(sents, index, cluster_infos, last_mentions):
    """Extract change features.

    Args:
        sents (list of `Span`): List of sentences in the document.
        index (int): Index of the current sentence.
        cluster_infos (dict of str:obj): Output of `extract_cluster_infos`.
        last_mentions (dict of int:int): Output of `extract_last_mentions`.
    
    Returns:
        dict of str:obj: Feature dict.
    
    """
    features = {}
    try:
        sent = sents[index]
        SPACE_NOUNS.provide()
        all_mentions = set()
        all_locations_infos = []
        all_others_infos = []
        for token in sent:
            if not is_direct_speech(token):

                # temponyms
                for temponym in token._.temponyms:
                    norm_val = temponym._.temponym_norm["NORM_VALUE"]
                    #substrings = [norm_val[i:j] for i in range(len(norm_val)) for j in range(i+1, len(norm_val)+1)]
                    substrings = []
                    substr = ""
                    for c in norm_val:
                        if len(substr) == 0:
                            substr += c
                        else:
                            if c == "-":
                                substrings.append(substr)
                                substr = ""
                            elif c.isupper() and substr[-1].islower():
                                substrings.append(substr)
                                substr = c
                            else:
                                substr += c
                    substrings.append(substr)
                    #print(norm_val, substrings)
                    for substr in substrings:
                        features["temponym_substr:" + substr] = True
                    features["temponym_type:" + temponym._.temponym_norm["TYPE"]] = True
                    if "NORM_MOD" in temponym._.temponym_norm:
                        features["temponym_mod:" + temponym._.temponym_norm["NORM_MOD"]] = True
                    if "NORM_QUANT" in temponym._.temponym_norm:
                        features["temponym_quant:" + temponym._.temponym_norm["NORM_QUANT"]] = True
                    if "NORM_FREQ" in temponym._.temponym_norm:
                        features["temponym_freq:" + temponym._.temponym_norm["NORM_FREQ"]] = True
                    features["has_temponym"] = True
                
                # mentions
                for cluster in token._.coref_clusters:
                    for current_mention in cluster.mentions:
                        if token in current_mention:
                            if current_mention not in all_mentions:
                                all_mentions.add(current_mention)
                                try:
                                    sent_dist = index-sents.index(sent.doc[last_mentions[current_mention.root.i]].sent)
                                except KeyError:
                                    sent_dist = -1
                                mention_info = (current_mention, sent_dist)
                                if is_space_ent(current_mention):
                                    all_locations_infos.append(mention_info)
                                else:
                                    all_others_infos.append(mention_info)
                            break
        
        all_locations_infos = sorted(all_locations_infos, key=lambda x: x[1])
        all_others_infos = sorted(all_others_infos, key=lambda x: x[1])
        all_xtype_infos = [all_locations_infos, all_others_infos]
        for n, xtype in enumerate(["space_", "other_"]):
            all_infos = all_xtype_infos[n]
            if len(all_infos) > 0:
                features["has_" + xtype] = True
                if len(all_infos) == 1:
                    features[xtype + "same"] = True
                for i, name in enumerate(["farest_", "nearest_"]):
                    if i == 0:
                        current_mention, sent_dist = all_infos[-1]
                    else:
                        current_mention, sent_dist = all_infos[0]

                    # mention features
                    features[xtype + name + "sent_dist"] = sent_dist
                    if is_PRON(current_mention):
                        features[xtype + name + "mention_is_pron"] = True
                    elif is_NE(current_mention):
                        features[xtype + name + "mention_is_ne"] = True
                    else:
                        features[xtype + name + "mention_is_nn"] = True
                    features[xtype + name + "mention_dep"] = current_mention.root.dep_.split(":")[0]
                    morph = current_mention.root._.morph
                    for val in morph.case_:
                        features[xtype + name + "mention_case:" + val] = True
                    for val in morph.person_:
                        features[xtype + name + "mention_person:" + val] = True
                    for val in morph.numerus_:
                        features[xtype + name + "mention_numerus:" + val] = True
                    for val in morph.gender_:
                        features[xtype + name + "mention_gender:" + val] = True
                    for word in current_mention:
                        if word.pos_ == "DET":
                            features[xtype + name + "mention_article"] = True
                            features[xtype + name + "mention_article_lemma:" + word.lemma_.lower()] = True
                        if word.pos_ == "NUM" or word.dep_.split(":")[0] == "nummod":
                            features[xtype + name + "mention_numeral"] = True
                        if word.lemma_ in ["beid", "beide", "einzig"]:
                            features[xtype + name + "mention_article"] = True
                            features[xtype + name + "mention_article_lemma:" + word.lemma_.lower()] = True
                            features[xtype + name + "mention_numeral"] = True

                    # cluster features
                    cluster_infos_ = cluster_infos[current_mention._.coref_cluster.i]
                    for info in cluster_infos_:
                        features[xtype + name + info] = cluster_infos_[info]
                
    except IndexError:
        pass
    return features


def extract_ngram_features(sents, index, max_len=1):
    """Extract token n-grams in a sentence.

    Args:
        sents (list of `Span`): List of sentences in the document.
        index (int): Index of the current sentence.
        max_len (int): Features are n-tuples; `max_len` specified the maximum length.
    
    Returns:
        dict of str:obj: Feature dict.
    
    """
    features = {}
    try:
        sent = sents[index]
        tokens = [token.lemma_.lower() for token in sent]
        feats = []
        for n in range(1, max_len+1):
            feats.extend([str(n)+"_"+str(x) for x in ngrams(tokens, n)])
        for feat in feats:
            features[feat] = True
    except IndexError:
        pass
    return features
