import os

# path to project root
ROOT_PATH = os.path.dirname(__file__)


# paths to resources for pipeline components

RESOURCES_PATH = os.path.join(ROOT_PATH, "resources")

global COREF_PATH
COREF_PATH = os.path.join(RESOURCES_PATH, "coreference_resolution")

global FLEXION_PATH
FLEXION_PATH = os.path.join(RESOURCES_PATH, "flexion")

global HEIDELTIME_PATH
HEIDELTIME_PATH = os.path.join(RESOURCES_PATH, "heideltime")

global NRC_EMOTIONS_PATH
NRC_EMOTIONS_PATH = os.path.join(RESOURCES_PATH, "NRC-Emotion-Lexicon-v0.92")

global PARSING_PATH
PARSING_PATH = os.path.join(RESOURCES_PATH, "parsing")

global SPACE_NOUNS_PATH
SPACE_NOUNS_PATH = os.path.join(RESOURCES_PATH, "space_nouns")

global WIKTIONARY_TOOLS_PATH
WIKTIONARY_TOOLS_PATH = os.path.join(RESOURCES_PATH, "wiktionary_tools")

global GERMANET_PATH
GERMANET_PATH = os.path.join(RESOURCES_PATH, ".", "germanet")


# path to texts

global PICKLE_PATH
PICKLE_PATH = os.path.join(ROOT_PATH, "pickles")

global SCENE_RESULTS
SCENE_RESULTS = os.path.join(ROOT_PATH, "main/scene_results")
