from pathlib import Path
from germanetpy.germanet import Germanet

data_path = str(Path.home()) + "/germanet/GN_V150/GN_V150_XML"
frequencylist_nouns = str(Path.home()) + "/germanet/GN_V150/FreqLists/noun_freqs_decow14_16.txt"
germanet = Germanet(data_path)

exclude = [
    germanet.get_synset_by_id("s42095"), #lexunits=Bereich, Welt, Feld
    germanet.get_synset_by_id("s19548"), #lexunits=Beruf, Berufstätigkeit, Berufsausübung, Erwerbsarbeit, Erwerbstätigkeit, Broterwerb, Tätigkeit, Berufsarbeit
    germanet.get_synset_by_id("s51150"), #lexunits=Zeitpunkt
    germanet.get_synset_by_id("s51652"), #lexunits=Befristung, Zeitbestimmung
    germanet.get_synset_by_id("s42845"), #lexunits=Himmel, Firmament
]


def append_to_space_nouns(synset,space_nouns):
    for lexunit in synset.lexunits:
        if lexunit.orthform not in space_nouns:
            space_nouns.append(lexunit.orthform)


# synset s41396, top level synset with lexunits=Stelle, Ort, Stätte
space_nouns = list()
for synset_0 in germanet.get_synset_by_id('s41396').direct_hyponyms:
    #print(synset)
    #print(synset.id)
    if synset_0 not in exclude:
        append_to_space_nouns(synset_0, space_nouns)
        for synset_1 in synset_0.direct_hyponyms:
            #print(synset_1)
            if synset_1 not in exclude:
                append_to_space_nouns(synset_1, space_nouns)
                for synset_2 in synset_1.direct_hyponyms:
                    if synset_2 not in exclude:
                        #print(synset_2)
                        append_to_space_nouns(synset_2, space_nouns)
                        for synset_3 in synset_2.direct_hyponyms:
                            if synset_3 not in exclude:
                                #print(synset_3)
                                append_to_space_nouns(synset_3, space_nouns)
                                for synset_4 in synset_3.direct_hyponyms:
                                    if synset_4 not in exclude:
                                        #print(synset_4)
                                        append_to_space_nouns(synset_4, space_nouns)
                                        for synset_5 in synset_4.direct_hyponyms:
                                            if synset_5 not in exclude:
                                                #print(synset_5)
                                                append_to_space_nouns(synset_5, space_nouns)
                                                for synset_6 in synset_5.direct_hyponyms:
                                                    if synset_6 not in exclude:
                                                        #print(synset_6)
                                                        append_to_space_nouns(synset_6, space_nouns)
                                                        for synset_7 in synset_6.direct_hyponyms:
                                                            if synset_7 not in exclude:
                                                                #print(synset_7)
                                                                append_to_space_nouns(synset_7, space_nouns)
                                                                for synset_8 in synset_7.direct_hyponyms:
                                                                    if synset_8 not in exclude:
                                                                        #print(synset_8)
                                                                        append_to_space_nouns(synset_8, space_nouns)
                                                                        for synset_9 in synset_8.direct_hyponyms:
                                                                            if synset_9 not in exclude:
                                                                                #print(synset_9)
                                                                                append_to_space_nouns(synset_9, space_nouns)
                                                                                for synset_10 in synset_9.direct_hyponyms:
                                                                                    if synset_10 not in exclude:
                                                                                        #print(synset_10)
                                                                                        append_to_space_nouns(synset_10, space_nouns)
                                                                                        for synset_11 in synset_10.direct_hyponyms:
                                                                                            if synset_11 not in exclude:
                                                                                                print(synset_11)
                                                                                                append_to_space_nouns(synset_11, space_nouns)
# hyponyms of synset: s5682, lexunits=Bauwerk
for synset in germanet.get_synset_by_id("s5682").all_hyponyms():
    append_to_space_nouns(synset, space_nouns)

# hyponyms of synset: s9918, lexunits=Gebäudeteil
for synset in germanet.get_synset_by_id("s9918").all_hyponyms():
    append_to_space_nouns(synset, space_nouns)

f = open("space_nouns.txt", "w")
for el in sorted(space_nouns):
    f.write(el+"\n")
f.close()