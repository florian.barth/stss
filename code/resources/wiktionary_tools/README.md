# Wiktionary tools

This directory contains the scripts to extract perfect auxiliaries of German verbs from [Wiktionary](https://de.wikipedia.org/) as well as the output.

## Download resources

Download:

- complete dewiktionary dump from [https://dumps.wikimedia.org/dewiktionary/20200701/](https://dumps.wikimedia.org/dewiktionary/20200701/); download link: [https://dumps.wikimedia.org/dewiktionary/20200701/dewiktionary-20200701-pages-articles-multistream.xml.bz2](https://dumps.wikimedia.org/dewiktionary/20200701/dewiktionary-20200701-pages-articles-multistream.xml.bz2)

Save `dewiktionary-20200701-pages-articles-multistream.xml` in the same directory as `pipy`.

## Run scripts

Navigate into this directory and run the following commands:

```sh
python verb_tense.py
```

## Output

- `verb_tense.json`