import json
import os
from bz2file import BZ2File
from wiktionary_de_parser import Parser

# This script extracts tense auxilaries for German verbs from Wiktionary.
# Input: ../../../dewiktionary-20200701-pages-articles-multistream.xml.bz2 - The Wiktionary dump.
# Output: verb_tense.json - dictionary with the format {verb : [auxiliaries]}

version = "dewiktionary-20200701-pages-articles-multistream.xml.bz2"

path = os.path.join("..", "..", "..", version)

if os.path.exists(path):
    bz = BZ2File(path)
    dd = {}
    for record in Parser(bz):
        if 'langCode' not in record or record['langCode'] != 'de':
            continue
        if "Verb" in record["pos"] and len(record["pos"]) == 1 and "flexion" in record:
            hvs = []
            for hv in ["Hilfsverb", "Hilfsverb*"]:
                if hv not in record["flexion"]:
                    continue
                hv = record["flexion"][hv]
                if hv is None:
                    continue
                hv = hv.replace("}}", "")
                if hv in ["haben", "sein"]:
                    hvs.append(hv)
            dd[record["title"]] = hvs
    with open("verb_tense.json", "w") as f:
        json.dump(dd, f, ensure_ascii=False, indent=4, sort_keys=True)
else:
    print("Download Wiktionary dump " + version + " from https://dumps.wikimedia.org/dewiktionary/ !")
