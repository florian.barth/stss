# KONVENS 2021 Shared Task on Scene Segmentation

To use the classifier, data in the format from the Shared Task on Scene Segmentation (STSS) is needed. Trial data can be found here: [Link](http://lsx-events.informatik.uni-wuerzburg.de/stss-2021/task.html) 
The full dataset is under copyright. Paste all files to `data/test`. 

You can run our NLP-pipeline and the classifier within a docker image, first build the image via `sh build.sh` and the run it with `sh run.sh`

## License/Citation

Our code is licensed under a **CC BY 4.0** license. If you use it, you should cite the following paper in your work:

> Florian Barth and Tillmann Dönicke (2021). "Participation in the KONVENS 2021 Shared Task on Scene Segmentation Using Temporal, Spatial and Entity Feature Vectors". In Proceedings of the Shared Task on Scene Segmentation at KONVENS 2021.
