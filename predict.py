import json
import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), "code"))
from pipeline import pipeline
from settings import *

if __name__ == "__main__":

    scene_corpus_path = os.path.join(os.path.dirname(__file__), "data")
    train_path = os.path.join(scene_corpus_path, "train")
    test_path = os.path.join(scene_corpus_path, "test")
    predictions_path = os.path.join(os.path.dirname(__file__), "predictions")

    pipe = pipeline(
        enable=["sentencizer", "analyzer", "clausizer", "tense_tagger", "speech_tagger", "speaker_extractor", "coref", "temponym_tagger", "germanet_tagger", "scene_tagger", "annotation_reader"], 
        sentencizer="scene_sentencizer", 
        corpus_path=test_path, 
        _train=None,
        pickle_load=True,
        pickle_save=True
    )

    for test_file in os.listdir(test_path):
        if not test_file.endswith(".json"):
            continue
        output_data = {}
        with open(os.path.join(test_path, test_file), "r") as f:
            input_data = json.load(f)
            output_data["text"] = input_data["text"]
            output_data["sentences"] = input_data["sentences"]
            output_data["scenes"] = []
            doc = pipe(input_data["text"])
            for scene in doc._.scenes:
                if scene._.scene_tag != "None":
                    start = doc[scene.start].idx
                    try:
                        end = doc[scene.end].idx
                    except IndexError:
                        end = len(doc)
                    output_data["scenes"].append({"begin" : start, "end" : end, "type" : scene._.scene_tag})
        # print([f for f in os.listdir('.')])
        # print([f for f in os.listdir('./data')])
        # print([f for f in os.listdir('./code')])
        with open(os.path.join(predictions_path, test_file), "w") as f:
            json.dump(output_data, f)
        # print([f for f in os.listdir('./predictions')])